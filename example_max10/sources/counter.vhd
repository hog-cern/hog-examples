
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @breif Definition of a simple counter.
--! The counter entity is a counter that counts down from a user dened value to '0'
--! The value from which to count down is set using the load_value port
--! The counter will assert limit each time it reaches 0
entity counter is 
  generic
  (
    nBits: natural := 21 --! size in bit of the counter 
  );
  port
  (
    clk: in std_logic ---! system clock
    ; rst: in std_logic --! reset signal will reste the copunter to 0
    ; enable: in std_logic --! enable signal: the counter will count only if eneble is '1'
    ; load_value: unsigned( nBits-1 downto 0) := to_unsigned(1250000, nBits) --! load value: the value from which to count down
    ; limit: out std_logic
  );
end;

architecture behaviour of counter is
  signal value: unsigned(nBits-1 downto 0) := to_unsigned(1250000, nBits);
  signal load: std_logic;
  constant zero: unsigned(nBits-1 downto 0) := to_unsigned(0, nBits);
begin

  limit <= load;

  load <= '1' when value = zero else '0';

  process(clk)
  begin
    if (rising_edge(clk) )
    then
      if (rst ='1')
      then
        value <= load_value;
      else
        if ( enable ='1' )
        then
          if load ='1'
          then
            value <= load_value - 1;
          else
            value <= value - 1;
          end if;
        end if;
      end if;
    end if;
  end process;

end behaviour;


