--   Copyright 2018-2020 The University of Birmingham
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http:--www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License

-- Doxygen-compatible comments
--! @file  top_example_max10.vhd
--! @brief top_example_max10 contains the definition of the top module for the example_max10 project
--! @details This is a bare bone quartus project
--! @author Nicolò Biesuz

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity example_max10 is
  generic (
  GLOBAL_DATE : std_logic_vector(31 downto 0);
  GLOBAL_TIME : std_logic_vector(31 downto 0);
  GLOBAL_VER  : std_logic_vector(31 downto 0);
  GLOBAL_SHA  : std_logic_vector(31 downto 0);
  TOP_VER     : std_logic_vector(31 downto 0);
  TOP_SHA     : std_logic_vector(31 downto 0);
  CON_VER     : std_logic_vector(31 downto 0);
  CON_SHA     : std_logic_vector(31 downto 0);
  HOG_VER     : std_logic_vector(31 downto 0);
  HOG_SHA     : std_logic_vector(31 downto 0);    

  EXAMPLE_MAX10_VER : std_logic_vector(31 downto 0);
  EXAMPLE_MAX10_SHA : std_logic_vector(31 downto 0)

);
port (
       clk : in std_logic;
       led : out std_logic_vector (3 downto 0) := x"0"
     );
end entity;  -- example_max10

architecture behaviour of example_max10 is
  component ALT_PLL IS
    PORT
    (
      inclk0	: IN STD_LOGIC  := '0';
      c0		  : OUT STD_LOGIC ;
      locked  : OUT STD_LOGIC 
    );
  end component ALT_PLL;

  component counter is 
    generic
    (
      nBits: natural := 21 --! size in bit of the counter 
    );
    port
    (
      clk: in std_logic ---! system clock
      ; rst: in std_logic --! reset signal will reste the copunter to 0
      ; enable: in std_logic --! enable signal: the counter will count only if eneble is '1'
      ; load_value: unsigned( nBits-1 downto 0) := to_unsigned(1250000, nBits) --! load value: the value from which to count down
      ; limit: out std_logic
    );
  end component counter;



  signal system_clock : std_logic := '0';
  signal pll_lock     : std_logic := '0';
  signal switch       : std_logic := '0';
begin

  ALT_PLL_inst: ALT_PLL
  PORT MAP
  (
    inclk0  => clk,
    c0		  => system_clock,
    locked	=> pll_lock
  );

  counter_inst: counter 
  generic map
  (
    nBits => 32
  )
  port map
  (
    clk         => system_clock,
    rst         => not pll_lock,
    enable      => pll_lock,
    load_value  => X"cafecafe",
    limit       => switch
  );


  process(system_clock)
  begin
    if rising_edge(system_clock)
    then
      led(0) <=  pll_lock;
      if switch = '1'
      then
        led(1) <=  not led(1);
      end if;
      led(2) <=  not led(1);
      led(3) <=  not led(0);
    end if;
  end process;

end architecture behaviour;
