library IEEE, example;
library other_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity top_fifo is
Generic (
  constant DATA_WIDTH  : positive := 18;
  constant FIFO_DEPTH	: positive := 256;
  -- Global Generic Variables
    GLOBAL_DATE : std_logic_vector(31 downto 0) := (others => '0');
    GLOBAL_TIME : std_logic_vector(31 downto 0) := (others => '0');
    GLOBAL_VER : std_logic_vector(31 downto 0) := (others => '0');
    GLOBAL_SHA : std_logic_vector(31 downto 0) := (others => '0');
    TOP_VER : std_logic_vector(31 downto 0) := (others => '0');
    TOP_SHA : std_logic_vector(31 downto 0) := (others => '0');
    CON_VER : std_logic_vector(31 downto 0) := (others => '0');
    CON_SHA : std_logic_vector(31 downto 0) := (others => '0');
    HOG_VER : std_logic_vector(31 downto 0) := (others => '0');
    HOG_SHA : std_logic_vector(31 downto 0) := (others => '0');
    FIFO_VER : std_logic_vector(31 downto 0) := (others => '0');
    FIFO_SHA : std_logic_vector(31 downto 0) := (others => '0');
    IPS_VER : std_logic_vector(31 downto 0) := (others => '0');
    IPS_SHA : std_logic_vector(31 downto 0) := (others => '0');
    IP_REPOSITORY_VER : std_logic_vector(31 downto 0) := (others => '0');
    IP_REPOSITORY_SHA : std_logic_vector(31 downto 0) := (others => '0');
    OTHER_LIB_VER : std_logic_vector(31 downto 0) := (others => '0');
    OTHER_LIB_SHA : std_logic_vector(31 downto 0) := (others => '0');
    XML_VER : std_logic_vector(31 downto 0) := (others => '0');
    XML_SHA : std_logic_vector(31 downto 0) := (others => '0');
    TEST_GENERIC : integer := 0
  );
Port (
  CLK		: in  STD_LOGIC;
  RST		: in  STD_LOGIC;
  WriteEn	: in  STD_LOGIC;
  DataIn	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
  ReadEn	: in  STD_LOGIC;
  DataOut	: out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
  Empty	: out STD_LOGIC;
  Full	: out STD_LOGIC
  );
end top_fifo;

architecture Behavioral of top_fifo is
signal fifo_out : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
signal adder_out : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');

COMPONENT fifo_generator_0
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;

begin

fifo1 : fifo_generator_0
PORT MAP (
    clk => clk,
    rst => RST,
    din => DataIn,
    wr_en => WriteEn,
    rd_en => ReadEn,
    dout => fifo_out,
    full => Full,
    empty => Empty
    );

adder_gen : if TEST_GENERIC = 0 generate
  adder : entity work.adder
generic map(
   DATA_WIDTH => DATA_WIDTH
   )
port map(
    clk => clk,
    DataIn => fifo_out,
    DataOut => DataOut
    );
else generate
  adder2 : entity other_lib.different_adder
generic map(
   DATA_WIDTH => DATA_WIDTH
   )
port map(
    clk => clk,
    DataIn => fifo_out,
    DataOut => DataOut
    );
end generate adder_gen;






end Behavioral;
