library IEEE, example;
library other_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity top_example is
Generic (
  constant DATA_WIDTH  : positive := 20;
  constant FIFO_DEPTH	: positive := 256
  );
Port (
  CLK		: in  STD_LOGIC;
  RST		: in  STD_LOGIC;
  WriteEn	: in  STD_LOGIC;
  DataIn	: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
  ReadEn	: in  STD_LOGIC;
  DataOut	: out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
  Empty	: out STD_LOGIC;
  Full	: out STD_LOGIC
  );
end top_example;

architecture Behavioral of top_example is
signal fifo_out : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
signal adder_out : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
signal memraddr, memwaddr : std_logic_vector(9 downto 0);
signal memre, memwe : std_logic;

begin

fifo : entity example.module_fifo_regs_no_flags
Generic map(
    g_WIDTH => DATA_WIDTH,
    g_DEPTH => FIFO_DEPTH
)
PORT MAP (
    i_rst_sync => RST,
    i_clk => clk,
    i_wr_en => WriteEn,
    i_wr_data => DataIn,
    o_full => Full,
    i_rd_en => ReadEn,
    o_rd_data => fifo_out,
    o_empty => Empty
);

adder : entity example.adder
generic map(
   DATA_WIDTH => DATA_WIDTH
   )
port map(
    clk => clk,
    DataIn => fifo_out,
    DataOut => adder_out
    );

adder2 : entity other_lib.different_adder
generic map(
   DATA_WIDTH => DATA_WIDTH
   )
port map(
    clk => clk,
    DataIn => adder_out,
    DataOut => DataOut
    );


end Behavioral;
