library IEEE, fifo;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity top_proj is
  generic (
    constant DATA_WIDTH : positive := 18;
    constant FIFO_DEPTH : positive := 256;
    -- Global Generic Variables
    GLOBAL_DATE : std_logic_vector(31 downto 0) := (others => '0');
    GLOBAL_TIME : std_logic_vector(31 downto 0) := (others => '0');
    GLOBAL_VER : std_logic_vector(31 downto 0) := (others => '0');
    GLOBAL_SHA : std_logic_vector(31 downto 0) := (others => '0');
    TOP_VER : std_logic_vector(31 downto 0) := (others => '0');
    TOP_SHA : std_logic_vector(31 downto 0) := (others => '0');
    CON_VER : std_logic_vector(31 downto 0) := (others => '0');
    CON_SHA : std_logic_vector(31 downto 0) := (others => '0');
    HOG_VER : std_logic_vector(31 downto 0) := (others => '0');
    HOG_SHA : std_logic_vector(31 downto 0) := (others => '0');
    FIFO_VER : std_logic_vector(31 downto 0) := (others => '0');
    FIFO_SHA : std_logic_vector(31 downto 0) := (others => '0');
    IPS_VER : std_logic_vector(31 downto 0) := (others => '0');
    IPS_SHA : std_logic_vector(31 downto 0) := (others => '0');
    --
    FLAVOUR : integer := 0
    );
  port (
    CLK     : in  std_logic;
    RST     : in  std_logic;
    WriteEn : in  std_logic;
    DataIn  : in  std_logic_vector (DATA_WIDTH - 1 downto 0);
    ReadEn  : in  std_logic;
    DataOut : out std_logic_vector (DATA_WIDTH - 1 downto 0);
    Empty   : out std_logic;
    Full    : out std_logic
    );
end top_proj;

architecture Behavioral of top_proj is
  signal fifo_out  : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
  signal adder_out : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');

COMPONENT fifo_generator_0
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;

begin
  fifo : fifo_generator_0
    port map (
      clk   => clk,
      rst  => RST,
      din   => DataIn,
      wr_en => WriteEn,
      rd_en => ReadEn,
      dout  => fifo_out,
      full  => Full,
      empty => Empty
      );

  flavour_gen : if FLAVOUR = 0 generate
    adder : entity work.adder
    generic map(
      DATA_WIDTH => DATA_WIDTH
      )
    port map(
      clk     => clk,
      DataIn  => fifo_out,
      DataOut => DataOut
      );
  else generate
    adder2 : entity work.different_adder
    generic map(
      DATA_WIDTH => DATA_WIDTH
      )
    port map(
      clk     => clk,
      DataIn  => fifo_out,
      DataOut => DataOut
      );
  end generate flavour_gen;




end Behavioral;
