--Copyright 1986-2023 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2022.2.2 (lin64) Build 3788238 Tue Feb 21 19:59:23 MST 2023
--Date        : Thu May 11 13:57:13 2023
--Host        : epldt002.ph.bham.ac.uk running 64-bit CentOS Linux release 7.9.2009 (Core)
--Command     : generate_target soc_vip_wrapper.bd
--Design      : soc_vip_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity soc_vip_wrapper is
  port (
    --BRAM_PORTB_0_addr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    --BRAM_PORTB_0_clk : in STD_LOGIC;
    --BRAM_PORTB_0_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    --BRAM_PORTB_0_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
    --BRAM_PORTB_0_en : in STD_LOGIC;
    --BRAM_PORTB_0_rst : in STD_LOGIC;
    --BRAM_PORTB_0_we : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ch0_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch0_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch0_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_reset_n : out STD_LOGIC;
    ch1_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch1_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch1_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_reset_n : out STD_LOGIC;
    lpddr4_clk1_clk_n : in STD_LOGIC;
    lpddr4_clk1_clk_p : in STD_LOGIC;
    pl0_ref_clk : out STD_LOGIC;
    pl_gen_reset : out STD_LOGIC
  );
end soc_vip_wrapper;

architecture STRUCTURE of soc_vip_wrapper is
  component soc_vip is
  port (
    ch0_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch0_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_reset_n : out STD_LOGIC;
    ch1_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch1_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_reset_n : out STD_LOGIC;
    lpddr4_clk1_clk_p : in STD_LOGIC;
    lpddr4_clk1_clk_n : in STD_LOGIC;
    BRAM_PORTB_0_addr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    BRAM_PORTB_0_clk : in STD_LOGIC;
    BRAM_PORTB_0_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_0_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
    BRAM_PORTB_0_en : in STD_LOGIC;
    BRAM_PORTB_0_rst : in STD_LOGIC;
    BRAM_PORTB_0_we : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl0_ref_clk : out STD_LOGIC;
    pl_gen_reset : out STD_LOGIC
  );
  end component soc_vip;
  
  signal minc : std_logic_vector(31 downto 0);
begin
soc_vip_i: component soc_vip
     port map (
      BRAM_PORTB_0_addr(12 downto 0) => (others => '0'), --BRAM_PORTB_0_addr(12 downto 0),
      BRAM_PORTB_0_clk => '0', --BRAM_PORTB_0_clk,
      BRAM_PORTB_0_din(31 downto 0) => (others => '0'),--BRAM_PORTB_0_din(31 downto 0),
      BRAM_PORTB_0_dout(31 downto 0) => minc, --BRAM_PORTB_0_dout(31 downto 0),
      BRAM_PORTB_0_en => '0', --BRAM_PORTB_0_en,
      BRAM_PORTB_0_rst => '0', --BRAM_PORTB_0_rst,
      BRAM_PORTB_0_we(3 downto 0) => (others => '0'), --BRAM_PORTB_0_we(3 downto 0),
      ch0_lpddr4_trip1_ca_a(5 downto 0) => ch0_lpddr4_trip1_ca_a(5 downto 0),
      ch0_lpddr4_trip1_ca_b(5 downto 0) => ch0_lpddr4_trip1_ca_b(5 downto 0),
      ch0_lpddr4_trip1_ck_c_a => ch0_lpddr4_trip1_ck_c_a,
      ch0_lpddr4_trip1_ck_c_b => ch0_lpddr4_trip1_ck_c_b,
      ch0_lpddr4_trip1_ck_t_a => ch0_lpddr4_trip1_ck_t_a,
      ch0_lpddr4_trip1_ck_t_b => ch0_lpddr4_trip1_ck_t_b,
      ch0_lpddr4_trip1_cke_a => ch0_lpddr4_trip1_cke_a,
      ch0_lpddr4_trip1_cke_b => ch0_lpddr4_trip1_cke_b,
      ch0_lpddr4_trip1_cs_a => ch0_lpddr4_trip1_cs_a,
      ch0_lpddr4_trip1_cs_b => ch0_lpddr4_trip1_cs_b,
      ch0_lpddr4_trip1_dmi_a(1 downto 0) => ch0_lpddr4_trip1_dmi_a(1 downto 0),
      ch0_lpddr4_trip1_dmi_b(1 downto 0) => ch0_lpddr4_trip1_dmi_b(1 downto 0),
      ch0_lpddr4_trip1_dq_a(15 downto 0) => ch0_lpddr4_trip1_dq_a(15 downto 0),
      ch0_lpddr4_trip1_dq_b(15 downto 0) => ch0_lpddr4_trip1_dq_b(15 downto 0),
      ch0_lpddr4_trip1_dqs_c_a(1 downto 0) => ch0_lpddr4_trip1_dqs_c_a(1 downto 0),
      ch0_lpddr4_trip1_dqs_c_b(1 downto 0) => ch0_lpddr4_trip1_dqs_c_b(1 downto 0),
      ch0_lpddr4_trip1_dqs_t_a(1 downto 0) => ch0_lpddr4_trip1_dqs_t_a(1 downto 0),
      ch0_lpddr4_trip1_dqs_t_b(1 downto 0) => ch0_lpddr4_trip1_dqs_t_b(1 downto 0),
      ch0_lpddr4_trip1_reset_n => ch0_lpddr4_trip1_reset_n,
      ch1_lpddr4_trip1_ca_a(5 downto 0) => ch1_lpddr4_trip1_ca_a(5 downto 0),
      ch1_lpddr4_trip1_ca_b(5 downto 0) => ch1_lpddr4_trip1_ca_b(5 downto 0),
      ch1_lpddr4_trip1_ck_c_a => ch1_lpddr4_trip1_ck_c_a,
      ch1_lpddr4_trip1_ck_c_b => ch1_lpddr4_trip1_ck_c_b,
      ch1_lpddr4_trip1_ck_t_a => ch1_lpddr4_trip1_ck_t_a,
      ch1_lpddr4_trip1_ck_t_b => ch1_lpddr4_trip1_ck_t_b,
      ch1_lpddr4_trip1_cke_a => ch1_lpddr4_trip1_cke_a,
      ch1_lpddr4_trip1_cke_b => ch1_lpddr4_trip1_cke_b,
      ch1_lpddr4_trip1_cs_a => ch1_lpddr4_trip1_cs_a,
      ch1_lpddr4_trip1_cs_b => ch1_lpddr4_trip1_cs_b,
      ch1_lpddr4_trip1_dmi_a(1 downto 0) => ch1_lpddr4_trip1_dmi_a(1 downto 0),
      ch1_lpddr4_trip1_dmi_b(1 downto 0) => ch1_lpddr4_trip1_dmi_b(1 downto 0),
      ch1_lpddr4_trip1_dq_a(15 downto 0) => ch1_lpddr4_trip1_dq_a(15 downto 0),
      ch1_lpddr4_trip1_dq_b(15 downto 0) => ch1_lpddr4_trip1_dq_b(15 downto 0),
      ch1_lpddr4_trip1_dqs_c_a(1 downto 0) => ch1_lpddr4_trip1_dqs_c_a(1 downto 0),
      ch1_lpddr4_trip1_dqs_c_b(1 downto 0) => ch1_lpddr4_trip1_dqs_c_b(1 downto 0),
      ch1_lpddr4_trip1_dqs_t_a(1 downto 0) => ch1_lpddr4_trip1_dqs_t_a(1 downto 0),
      ch1_lpddr4_trip1_dqs_t_b(1 downto 0) => ch1_lpddr4_trip1_dqs_t_b(1 downto 0),
      ch1_lpddr4_trip1_reset_n => ch1_lpddr4_trip1_reset_n,
      lpddr4_clk1_clk_n => lpddr4_clk1_clk_n,
      lpddr4_clk1_clk_p => lpddr4_clk1_clk_p,
      pl0_ref_clk => pl0_ref_clk,
      pl_gen_reset => pl_gen_reset
    );
end STRUCTURE;
